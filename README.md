# winter

the snowflake python library you didn't know you needed

## install

```sh
pipenv install --editable git+https://gitlab.com/elixire/winter.git#egg=winter
```

**TODO: add setup.py instructions**

## usage

```python
from winter import SnowflakeFactory

# By default, the factory is loaded with Discord's snowflake epoch.
# It is possible to use a different one.
factory = SnowflakeFactory()

my_snowflake = factory.snowflake()
posix_timestamp = factory.to_timestamp(my_snowflake)
```

## development/testing

```sh
tox
```
