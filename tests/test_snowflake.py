# winter: helper library for snowflakes
# Copyright 2020-2021, elixi.re Team and the winter contributors
# SPDX-License-Identifier: MIT

import time
import datetime

from winter import SnowflakeFactory


def test_snowflake_simple():
    """Simple snowflake test"""

    factory = SnowflakeFactory()

    sflake = factory.snowflake()

    assert isinstance(sflake, int)

    # since we're making the timestamps from a single snowflake, there is no
    # worry that the equality assertion fails.

    sflake_timestamp = factory.to_timestamp(sflake)
    assert isinstance(sflake_timestamp, float)

    sflake_datetime = factory.to_datetime(sflake)
    assert isinstance(sflake_datetime, datetime.datetime)

    assert sflake_datetime.timestamp() == sflake_timestamp


TEST_OVERFLOW_AMOUNT = 6000


def test_sflake_overflow():
    """Ensure snowflake library doesn't fall on a regression after
    generating ~4096 snowflakes.

    This bug was caused by the internal counter
    variable overflowing and causing time jumps in snowflakes.

    for reference, see
    https://gitlab.com/elixire/elixire/-/commit/942d0a424386dfba3873b57ecc223aff8f991259
    """
    factory = SnowflakeFactory(counter=0)
    last_id = factory.snowflake()

    for _ in range(TEST_OVERFLOW_AMOUNT):
        new_id = factory.snowflake()

        # the difference between last_id and new_id can't be too great.
        # thats how the time travel bug happens.
        #
        # we can't just do 'delta > 1' because that only works for snowflakes
        # generated too fast
        #
        # after a while timestamp will bump and then we have a much greater
        # difference in time (around 4194305)
        #
        # i bump the check to 10000000 because our overflow bug was causing
        # deltas at much greater orders of magnitude, and that's what we
        # want to catch here
        delta = new_id - last_id
        if delta > 10000000:
            raise RuntimeError(f"delta too great: {delta}")

        # update last_id to keep up
        last_id = new_id
