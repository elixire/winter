# -*- coding: utf-8 -*-

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

readme = ""

setup(
    long_description=readme,
    name="winter",
    version="0.1.0",
    description="Discord snowflake library",
    python_requires="==3.*,>=3.7.0",
    author="elixi.re",
    author_email="python@elixi.re",
    license="MIT",
    packages=["winter"],
    package_dir={"": "."},
    package_data={"winter": ["py.typed"]},
    install_requires=[],
)
