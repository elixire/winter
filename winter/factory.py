# winter: helper library for snowflakes
# Copyright 2020-2021, elixi.re Team and the winter contributors
# SPDX-License-Identifier: MIT

import time
import datetime

DISCORD_EPOCH = 1420070400000


class SnowflakeFactory:
    __slots__ = ("process_id", "worker_id", "counter", "epoch")

    def __init__(
        self,
        *,
        process_id: int = 0,
        worker_id: int = 0,
        epoch: int = DISCORD_EPOCH,
        counter: int = 0,
    ):
        self.process_id = process_id
        self.worker_id = worker_id
        self.epoch = epoch
        self.counter = counter

    def from_timestamp(
        self,
        timestamp: float,
        counter: int,
    ) -> int:
        """Convert a timestamp to a snowflake.

        This function does not modify internal state of the factory, hence
        why you have to give the current counter. For a general-purpose easy
        function to get snowflakes, use :func:`SnowflakeFactory.snowflake`.

        Arguments
        ---------
        timestamp: float
            Timestamp to be fed in to the snowflake algorithm.
            This timestamp SHOULD be an UNIX timestamp with
                millisecond precision.
            The value of :func:`time.time` serves this purpose well.
        counter: int
            The counter used in the snowflake.
        """
        timestamp *= 1000
        epochized = int(timestamp) - self.epoch

        # TODO: validate possible overflows on epochized

        # 22 bits to insert the other variables
        sflake = epochized << 22

        sflake |= (self.worker_id % 32) << 17
        sflake |= (self.process_id % 32) << 12
        sflake |= counter % 4096
        return sflake

    def to_timestamp(self, snowflake: int) -> float:
        """Get the UNIX timestamp(with millisecond precision, as a float)
        from a specific snowflake.
        """
        # bits 22 and onward encode epochized
        epochized = snowflake >> 22

        # since epochized is the time *since* the EPOCH
        # the unix timestamp will be the time *plus* the EPOCH
        timestamp = epochized + self.epoch

        # convert it to seconds (to follow the same interface as time.time())
        return timestamp / 1000

    def to_datetime(self, snowflake: int) -> datetime.datetime:
        """Return a datetime object representing the snowflake."""
        unix_ts = self.to_timestamp(snowflake)
        return datetime.datetime.fromtimestamp(unix_ts)

    def snowflake(self) -> int:
        """Generate a snowflake. This is the preffered method to use."""
        snowflake = self.from_timestamp(time.time(), self.counter)
        self.counter += 1
        return snowflake
